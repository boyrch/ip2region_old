#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
This sub-module contains functions that might be usefull to any other
sub-module or script.
"""

import ip2Region
import pickle







def GetCityLocation(ip, count=0):
    try:
        target = ip.strip()
        dbFile = "ip2region.db"
        algorithm = "b-tree"
        #algorithm == "binary"
        #algorithm == "memory"
        searcher = ip2Region.Ip2Region(dbFile)
        if not searcher.isip(target):
            logging.warn(target + " is not valid ip")
            return ['unknown', 'unknown', 'unknown', 'unknown', '0', '0']
        else:
            if target == "":
                return ['unknown', 'unknown', 'unknown', 'unknown', '0', '0']
            if algorithm == "b-tree":
                data = searcher.btreeSearch(target)
            elif algorithm == "binary":
                data = searcher.binarySearch(target)
            else:
                data = searcher.memorySearch(target)
            if isinstance(data, dict):
                loc = data["region"].decode('utf-8').split('|')
                return [loc[1], loc[0], loc[2], loc[3], '0', '0']
            else:
                return ['unknown', 'unknown', 'unknown', 'unknown', '0', '0']
        searcher.close()
    except Exception as e:
        logging.warn(e)
        return ['unknown', 'unknown', 'unknown', 'unknown', '0', '0']

    """    
    ipint = ip2int(ip)

    SerSQL = "SELECT * FROM (SELECT *   FROM `location` WHERE `endIpNum` > %d LIMIT 0,5) AS t1,(SELECT * FROM `location` WHERE `startIpNum` < %d ORDER BY startIpNum DESC LIMIT 0,5) AS t2 WHERE t1.network = t2.network limit 0,1" % (ipint, ipint)
    #SerSQL = "select country_iso_code,country_name,province_name,city_name,latitude,longitude from location where endIpNum>%d limit 1" % (ipint)
    c, data_one, data_all = SqlUpdate(SerSQL)
    if c == 0:
        if count < 1:
            return GetCityLocation(ip, count + 1)
        else:
            return ['unknown', 'unknown', 'unknown', 'unknown', '0', '0']
    else:
        return [data_one["country_iso_code"], data_one["country_name"], data_one["province_name"], data_one["city_name"], data_one["latitude"], data_one["longitude"]]
    """

"""
    TODO:定义存储的JSON格式
"""

print(GetCityLocation("1.0.63.110"))
